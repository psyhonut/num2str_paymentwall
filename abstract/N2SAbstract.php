<?php
/**
 * User: psyhonut
 * Date: 23.09.2014
 */

namespace n2s;

abstract class N2SAbstract
{
    /**
     * @param $language
     *
     * @throws \UserException
     */
    public static function lang($language)
    {
        $className = ucfirst($language);
        $fileName = BASE_DIR . DS . 'formats' . DS . $className . '.php';

        if (file_exists($fileName)) {
            require_once($fileName);

            $className = __NAMESPACE__ . '\\' . $className;
            return new $className();
        } else {
            throw new \UserException('N2S Language @' . $language  . '@ Not Found');
        }
    }
} 