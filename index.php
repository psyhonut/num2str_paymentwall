<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>



<?php
/**
 * User: psyhonut
 * Date: 23.09.2014
 */

define('BASE_DIR', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);

spl_autoload_register(
    function ($class) {
        $class = str_replace('n2s', '', $class); // :)
        $folders = array('.', 'abstract', 'exceptions', 'formats', 'interface');
        foreach($folders AS $folder) {
            $file = $folder . DS . $class . '.php';
            if(file_exists($file)) {
                require_once $file;
            }
        }
    }
);

use n2s\N2S;

$number = 320; // до миллиарда
$language = 'ua'; // 'ru', 'ua'


try {
    $n2s = N2S::lang($language);

    echo '"' . $number . '"' . ' прописью на языке "' . $language . '" это: "' . $n2s->makeString($number) . '"';
} catch (Exception $e) {
    echo 'ERROR! ' . $e->getMessage();
}
