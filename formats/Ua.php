<?php
/**
 * User: psyhonut
 * Date: 23.09.2014
 */

namespace n2s;

class Ua extends Source
{
    public function makeString($num)
    {
        $num = sprintf("%015.2f", floatval($num));

        $nul = 'ноль';
        $ten = array(
            array('', 'один', 'два', 'три', 'чотири', 'п\'ять', 'шiсть', 'сiм', 'вiсiм', 'дев\'ять'),
            array('', 'одна', 'двi', 'три', 'чотири', 'п\'ять', 'шiсть', 'сiм', 'вiсiм', 'дев\'ять'),
        );
        $a20 = array('десять', 'одинадцять', 'дванадцять', 'тринадцять', 'чотирнадцять', 'п\'ятнадцать', 'шiстнадцять', 'сiмнадцять', 'вiсiмнадцять',
                     'дев\'ятнадцять');
        $tens = array(2 => 'двадцять', 'тридцять', 'сорок', 'пятьдесят', 'шiстьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $hundred = array('', 'сто', 'двiстi', 'триста', 'чотириста', 'п\'ятьсот', 'шiстьсот', 'сiмсот', 'вiсiмсот', 'дев\'ятьсот');
        $unit = array( // Units
                       array('', '', '', 1),
                       array('', '', '', 0),
                       array('тисяча', 'тисячi', 'тисяч', 1),
                       array('мiльйон', 'мiльйони', 'мiльйонiв', 0),
                       array('мiльярд', 'мiльярда', 'мiльярдiв', 0),
        );
        //
        $out = array();
        if (intval($num) > 0) {
            foreach (str_split($num, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v)) {
                    continue;
                }
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) {
                    $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3];
                } # 20-99
                else {
                    $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3];
                } # 10-19 | 1-9
                if ($uk > 1) {
                    $out[] = $this->morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
                }
            } //foreach
        } else {
            $out[] = $nul;
        }
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

}